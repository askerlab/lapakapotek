	
	<!-- breadcrumb -->
	<div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">
		<a href="<?= base_url() ?>" class="s-text16">
			Beranda
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<a href="<?= base_url()."product/category/{$kategori->slug}" ?>" class="s-text16">
			<?= $kategori->nama ?>
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<span class="s-text17">
			<?= $product->nama ?>
		</span>
	</div>

	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<?php if($images->num_rows() > 0): ?>
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>
					<div class="slick3">
						<?php foreach($images->result() as $image): ?>
						<div class="item-slick3" data-thumb="<?= base_url()."uploads/images/products/{$image->url}" ?>">
							<div class="wrap-pic-w">
								<img src="<?= base_url()."uploads/images/products/{$image->url}" ?>" alt="<?= $product->nama ?>">
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
					<?= $product->nama ?>
				</h4>

				<span class="m-text17">
					<?= toRupiah($product->harga); ?>
				</span>

				<p class="s-text8 p-t-10">
					<?= $product->deskripsi ?>
				</p>

				<!--  -->
				<div class="p-t-33 p-b-60">

					<div class="flex-r-m flex-w p-t-10">
						<div class="w-size16 flex-m flex-w">
							<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>

								<input class="size8 m-text18 t-center num-product" type="number" name="num-product" value="1" min="1">

								<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>

							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
									Add to Cart
								</button>
							</div>
						</div>
					</div>
				</div>

				<div class="p-b-45">
					<span class="s-text8">Kategori: <?= $kategori->nama ?></span>
				</div>

				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Deskripsi Produk
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<table class="table table-responsive table-bordered">
							<tbody>
								<tr>
									<td>Indikasi</td>
									<td><?= @$description->indikasi ?></td>
								</tr>
								<tr>
									<td>Komposisi</td>
									<td><?= @$description->komposisi ?></td>
								</tr>
								<tr>
									<td>Dosis</td>
									<td><?= @$description->dosis ?></td>
								</tr>
								<tr>
									<td>Cara Pemakaian</td>
									<td><?= @$description->cara_pemakaian ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Informasi Produk
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<table class="table table-responsive table-bordered">
							<tbody>
								<tr>
									<td>Efek Samping</td>
									<td><?= @$information->efek_samping ?></td>
								</tr>
								<tr>
									<td>Cara Penyimpanan</td>
									<td><?= @$information->cara_penyimpanan ?></td>
								</tr>
								<tr>
									<td>Principal</td>
									<td><?= @$user->nama ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>