	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(<?= base_url() ?>resources/images/heading-pages-06.jpg);">
		<h2 class="l-text2 t-center">
			Daftar Apotek
		</h2>
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Daftar Apotek
					</h3>

					<div class="row">
					<?php foreach($listMerchant as $merchant): ?>
					<div class="col-md-3">
						<div class="block2">
	            <div class="block2-img wrap-pic-w of-hidden pos-relative ">
	                <img src="<?php echo base_url()."uploads/images/merchant/{$merchant["logo"]}" ?>" alt="<?= $merchant["nama"] ?>">

	                <div class="block2-overlay trans-0-4">
	                    <div class="block2-btn-addcart w-size1 trans-0-4">
	                        <a href="<?= base_url(). "merchant/{$merchant["slug"]}" ?>" class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">Lihat Apotek</a>
	                    </div>
	                </div>
	            </div>

	            <div class="block2-txt p-t-20">
	                <a href="<?= base_url(). "merchant/{$merchant["slug"]}" ?>" class="block2-name dis-block s-text3 p-b-5">
	                   <strong><?= $merchant["nama"] ?></strong>
	                </a>

	                <span class="block2-price m-text6 p-r-5" style="text-transform: none">
	                  <?= $merchant["alamat"] ?>
	                </span>
	            </div>
	        	</div>
					</div>
	        <?php endforeach; ?>
	        </div>
				</div>
			</div>
		</div>
	</section>