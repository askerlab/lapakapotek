<?php

if ( ! function_exists('trimLower'))
{
	function trimLower($string)
	{
		$string = trim($string);
		$string = strtolower($string);

		return $string;
	}
}

if ( ! function_exists('toRupiah'))
{
	function toRupiah($number) 
	{
		return "Rp".number_format($number, 0, '.', '.');
	}
}