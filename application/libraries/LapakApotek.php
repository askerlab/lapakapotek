<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LapakApotek extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->model("QueryBuilder");
	}

	protected function defaultTemplate($view, $data = '')
	{
		$data["title"] = isset($data["title"]) ? sprintf("%s - Lapak Apotek", $data["title"]) : "Lapak Apotek";
		$kategori = $this->QueryBuilder->get("m_jenis_obat", "RAND()", "desc", 4);
		$data["categories"] = $kategori->result();
		$this->load->view("_templates/defaultHeader", $data);
		$this->load->view($view, $data);
		$this->load->view("_templates/defaultFooter", $data);
	}
}