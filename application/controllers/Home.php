<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/LapakApotek.php';

class Home extends LapakApotek {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data["title"] = "Beranda";
		$kategori = $this->QueryBuilder->get("m_jenis_obat", "nama" , "asc");
		$produk = $this->QueryBuilder->get("m_products", "RAND()");
		$data["kategori"] = $kategori->result();
		$data["produk"] = $produk->result();
		$this->defaultTemplate("home", $data);
	}

	public function halaman($view)
	{
		$view = trimLower($view);

		switch($view){
			case "tentang": $this->about(); break;
			case "cara-belanja": $this->howToBuy(); break;
			case "merchant": $this->merchant(); break;
			default: redirect(); break;
		}
	}

	private function about()
	{
		$data["title"] = "Tentang Kami";
		$this->defaultTemplate("pages/about", $data);
	}

	private function howToBuy()
	{
		$data["title"] = "Cara Belanja";
		$this->defaultTemplate("pages/cara_belanja", $data);
	}

	private function merchant()
	{
		$list = $this->QueryBuilder->get("m_apotek", "RAND()");

		$result = [];
		foreach($list->result() as $merchant) {
			$user = $this->QueryBuilder->select(["id" => $merchant->id_user], "m_users")->row();
			$kota = $this->QueryBuilder->select(["id" => $merchant->id_kota], "m_kota")->row();
			$provinsi = $this->QueryBuilder->select(["id" => $merchant->id_provinsi], "m_provinsi")->row();
			$result[] = [
				"nama" => $user->nama,
				"slug" => $merchant->slug,
				"logo" => $merchant->logo,
				"alamat" => sprintf("%s, %s, %s", $merchant->alamat, $kota->nama, $provinsi->nama)
			];
		}
		$data["title"] = "Daftar Apotek";
		$data["listMerchant"] = $result;
		$this->defaultTemplate("pages/list_merchant", $data);
	}
}
