<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/LapakApotek.php';

class Products extends LapakApotek {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() { redirect(); }

	public function manage($action, $value)
	{
		$actionList = ["category", "detail"];
		if(!in_array( trimLower($action), $actionList)) redirect();
		else $this->$action($value);
	}

	private function category($value)
	{
		echo $value;
	} 

	private function detail($value)
	{
		$condition["slug"] = $value;
		$detail = $this->QueryBuilder->select($condition, "m_products");
		$row = $detail->num_rows();
		if( ! $row > 0) redirect();
		else {
			$data["product"] = $detail->row();
			$data["kategori"] = $this->QueryBuilder->select(["id" => $data["product"]->id_jenis], "m_jenis_obat")->row();
			$apotek = $this->QueryBuilder->select(["id" => $data["product"]->id_apotek], "m_apotek")->row();
			$user = $this->QueryBuilder->select(["id" => $apotek->id_user], "m_users")->row();
			$data["user"] = $user;
			$data["description"] = $this->QueryBuilder->select(["id_product" => $data["product"]->id], "t_product_description")->row();
			$data["images"] = $this->QueryBuilder->select(["id_product" => $data["product"]->id], "t_product_images");
			$data["information"] = $this->QueryBuilder->select(["id_product" => $data["product"]->id], "t_product_information")->row();
			$data["title"] = "Produk: {$data["product"]->nama}";
			$this->defaultTemplate("product/single", $data);
		}
	}
}

/* End of file Products.php */
/* Location: ./application/controllers/Products.php */