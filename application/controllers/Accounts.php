<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/LapakApotek.php';

class Accounts extends LapakApotek {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() { redirect(); }

	public function manage($action)
	{
		$action = trimLower($action);

		switch($action) {
			case "login": $this->login(); break;
			default: redirect(); break;
		}
	}

	private function login()
	{
		
	}
}

/* End of file Accounts.php */
/* Location: ./application/controllers/Accounts.php */